/*
 * vtk.cpp
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#include <QString>
#include <QStringList>
#include <QRegExp>

#include "vtk.h"

QString VTKReader::name(void) const
{
    return "VTK File Reader";
}

QStringList VTKReader::supportedMimetypes(void) const
{
    return QStringList() << "vtk" << "vtp";
}

QFile::FileError VTKReader::readData(void)
{
    int numberOfVertices = 0;
    int numberOfFaces = 0;
    bool ok;

    QString line;
    QStringList tmp;
    QFile file(m_filename);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return QFile::OpenError;
    }

    QTextStream in(&file);

    // before starting, make sure the mesh is empty
    m_mesh->clear();

    // and create some useful regexps
    QRegExp header("#\\s{0,1}vtk [\\s\\w]+ \\d\\.\\d", Qt::CaseInsensitive);
    QRegExp pdata("POINTS \\d+ float", Qt::CaseInsensitive);
    QRegExp fdata("POLYGONS \\d+ \\d+");
    QRegExp points("\\d+.\\d+ \\d+.\\d+ \\d+.\\d+");
    QRegExp faces("\\d+ \\d+ \\d+");

    // first line contains something like:
    //  # vtk DataFile Version x.y
    line = readLine(in);
    if(!header.exactMatch(line)) {
        return QFile::ResourceError;
    }

    // second line is the title
    readLine(in);

    // third line is ASCII or BINARY
    line = readLine(in);
    if(QString::compare(line, "ASCII", Qt::CaseInsensitive) != 0) {
        return QFile::ResourceError;
    }

    // then we have something like `DATASET <type>` and expect POLYDATA
    line = readLine(in);
    if(QString::compare(line, "DATASET POLYDATA", Qt::CaseInsensitive) != 0) {
        return QFile::ResourceError;
    }

    // next: POINTS <number> <type>
    // we suppose that `number` is an uint and `type` is always float.
    line = readLine(in);
    if(!pdata.exactMatch(line)) {
        return QFile::ResourceError;
    }

    tmp = line.split(" ");
    numberOfVertices = tmp[1].toInt(&ok);

    for(int i = 0; i < numberOfVertices; ++i) {
        line = readLine(in);

        if(!points.exactMatch(line)) {
            return QFile::ResourceError;
        }

        m_mesh->insert(createVertex(line));
    }

    // now find the polygons
    do {
        line = readLine(in);
    } while(!fdata.exactMatch(line));

    if(in.atEnd()) {
        return QFile::ResourceError;
    }

    tmp = line.split(" ");
    numberOfFaces = tmp[1].toInt(&ok);

    for(int i = 0; i < numberOfFaces; ++i) {
        line = readLine(in);

        if(!faces.exactMatch(line)) {
            return QFile::ResourceError;
        }

        m_mesh->insert(createFace(line));
    }

    // and finally set the twins for each halfedge, now that we have them all
    QList<HalfEdge *> halfEdges = m_mesh->halfEdges();
    for(int i = 0; i < halfEdges.size(); ++i) {
        halfEdges[i]->setTwin(halfEdges[i]->next()->origin()->leaving());
    }

    file.close();
    return QFile::NoError;
}

Vertex* VTKReader::createVertex(QString line)
{
    bool ok;
    QList<qreal> coords = toDoubleList(line);
    Vertex *v = new Vertex(coords[0], coords[1], coords[2]);

    return v;
}

HalfEdge* VTKReader::createHalfEdge(uint vindex, Face *face, HalfEdge *prev)
{
    HalfEdge *he = new HalfEdge();

    Vertex *v = m_mesh->vertex(vindex);
    v->setLeavingEdge(he);

    he->setOrigin(v);
    he->setFace(face);
    m_mesh->insert(he);

    if(prev) {
        prev->setNext(he);
    }

    return he;
}

Face* VTKReader::createFace(QString line)
{
    bool ok;
    Face *face = new Face();
    HalfEdge *current = NULL;
    HalfEdge *first;
    QList<int> vind = toIntList(line);

    // keep the first one
    current = first = createHalfEdge(vind[1], face, current);

    // take care of the middle ones
    for(int i = 2; i <= vind[0]; ++i) {
        current = createHalfEdge(vind[i], face, current);
    }

    // last one should link back to the first one
    current->setNext(first);

    return face;
}
