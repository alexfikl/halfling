/*
 * vtk.h
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#ifndef __VTK_H__
#define __VTK_H__

// Qt includes
#include <QObject>
#include <QPlugin>

#include "../../lib/imeshreader.h"

class VTKReader: public QObject, IMeshReader
{
    Q_OBJECT
    Q_INTERFACES(IMeshReader)

public:
    VTKReader():
        IMeshReader()
    {
    }

    VTKReader(Mesh *mesh, const QString &filename):
        IMeshReader(mesh, filename)
    {
    }

    QString name(void) const;
    QStringList supportedMimetypes(void) const;
    QFile::FileError readData(void);

private:
    Vertex* createVertex(QString line);
    HalfEdge* createHalfEdge(uint vindex, Face *face, HalfEdge *prev);
    Face* createFace(QString line);
};

Q_EXPORT_PLUGIN2(vtkreader, VTKReader)

#endif
