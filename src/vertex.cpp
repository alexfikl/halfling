/*
 * vertex.cpp
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#include "vertex.h"
#include "halfedge.h"

/*******************************************************
 *                  Constructors
 *******************************************************/
Vertex::Vertex():
    m_leaving(0)
{
}

Vertex::Vertex(const Vertex &other):
    m_id(other.id()), m_normal(other.normal()),
    m_coords(other.coords()), m_leaving(other.leaving())
{
}

Vertex::Vertex(qreal x, qreal y, qreal z, HalfEdge *leaving):
    m_coords(x, y, z), m_leaving(leaving)
{
}

Vertex::~Vertex()
{
}

/*******************************************************
 *                  Operators
 *******************************************************/

Vertex& Vertex::operator=(const Vertex &other)
{
    m_id = other.id();
    m_coords = other.coords();
    m_normal = other.normal();
    m_leaving = other.leaving();

    return *this;
}

bool Vertex::operator==(const Vertex &other) const
{
    return m_coords == other.coords();
}

/*******************************************************
 *                      Setters
 *******************************************************/

void Vertex::setId(uint id)
{
    m_id = id;
}

void Vertex::setX(qreal x)
{
    m_coords.setX(x);
}

void Vertex::setY(qreal y)
{
    m_coords.setY(y);
}

void Vertex::setZ(qreal z)
{
    m_coords.setZ(z);
}

void Vertex::setCoords(const Vector3D &coords)
{
    m_coords = coords;
}

void Vertex::setNormal(const Vector3D &normal)
{
    m_normal = normal;
}

void Vertex::setLeavingEdge(HalfEdge *leaving)
{
    m_leaving = leaving;
}

/*******************************************************
 *                      Getters
 *******************************************************/

uint Vertex::id(void) const
{
    return m_id;
}

qreal Vertex::x(void) const
{
    return m_coords.x();
}

qreal Vertex::y(void) const
{
    return m_coords.y();
}

qreal Vertex::z(void) const
{
    return m_coords.z();
}

Vector3D Vertex::coords(void) const
{
    return m_coords;
}

Vector3D Vertex::normal(void) const
{
    return m_normal;
}

HalfEdge* Vertex::leaving(void) const
{
    return m_leaving;
}

/*******************************************************
 *                      Others
 *******************************************************/

void Vertex::scale(qreal scale)
{
    m_coords /= scale;
}

bool Vertex::isConsistent(void) const
{
    if(m_leaving == NULL || m_leaving->origin() != this) {
        return false;
    }

    return true;
}

/*******************************************************
 *                      Static
 *******************************************************/

qreal Vertex::distance(const Vertex &u, const Vertex &v)
{
    Vector3D diff = u.coords() - v.coords();

    return diff.length();
}

Vector3D Vertex::direction(const Vertex &u, const Vertex &v)
{
    Vector3D diff = u.coords() - v.coords();

    return diff.normalized();
}
