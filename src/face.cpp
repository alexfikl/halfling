/*
 * face.cpp
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#include "vertex.h"
#include "halfedge.h"
#include "face.h"

/*******************************************************
 *                  Constructors
 *******************************************************/

Face::Face():
    m_edge(0)
{
}

Face::Face(HalfEdge *edge):
    m_edge(edge)
{
    update();
}

Face::~Face()
{
}

/*******************************************************
 *                  Operators
 *******************************************************/

Face& Face::operator=(Face &face)
{
    m_id = face.id();
    m_normal = face.normal();
    m_edge = face.edge();

    return (*this);
}

/*******************************************************
 *              Setters and Getters
 *******************************************************/

void Face::setId(uint id)
{
    m_id = id;
}

uint Face::id(void) const
{
    return m_id;
}

void Face::setEdge(HalfEdge *edge)
{
    m_edge = edge;
    update();
}

HalfEdge* Face::edge(void)
{
    return m_edge;
}

Vector3D Face::normal(void) const
{
    return m_normal;
}

/*******************************************************
 *                  Others
 *******************************************************/

int Face::edgeCount(void) const
{
    int count = 0;

    if(!m_edge) {
        return count;
    }

    HalfEdge *it = m_edge->next();
    while(it != m_edge) {
        ++count;
        it = it->next();
    }

    return count;
}

bool Face::isConsistent(void) const
{
    if(m_edge == NULL || m_edge->face() != this) {
        return false;
    }

    return true;
}

int Face::update(void)
{
    if(!m_edge) {
        m_normal = Vector3D();
        return 1;
    }

    Vector3D back, forward;
    HalfEdge *it = m_edge;

    m_normal = Vector3D();

    // update face normal
    forward = it->twin()->originCoords() - it->originCoords();
    it = it->next();

    while(it != m_edge) {
        back = -1 * forward;
        forward = it->twin()->originCoords() - it->originCoords();
        m_normal += Vector3D::crossProduct(forward, back);
        it = it->next();
    }

    back = -1 * forward;
    forward = it->twin()->originCoords() - it->originCoords();
    m_normal += Vector3D::crossProduct(forward, back);
    m_normal.normalize();

    // using the new face normal, update the normal for each vertex.
    it = m_edge;

    while(it != m_edge) {
        it->origin()->setNormal(it->origin()->normal() + m_normal);
        it = it->next();
    }

    return 0;
}
