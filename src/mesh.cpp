/*
 * mesh.cpp
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#include "mesh.h"

const Face* Mesh::InfiniteFace = new Face();

Mesh::Mesh()
{
    m_boundingBox.first = Vector3D(-1, -1, -1);
    m_boundingBox.second = Vector3D(1, 1, 1);
}

Mesh::Mesh(Mesh &other):
    m_vertices(other.vertices()), m_halfEdges(other.halfEdges()),
    m_faces(other.faces()), m_boundingBox(other.boundingBox())
{
}

Mesh::~Mesh()
{
    clear();
}

Mesh& Mesh::operator=(Mesh &other)
{
    m_vertices = other.vertices();
    m_halfEdges = other.halfEdges();
    m_faces = other.faces();

    m_boundingBox = other.boundingBox();

    return (*this);
}

int Mesh::numberOfVertices(void) const
{
    return m_vertices.size();
}

int Mesh::numberOfHalfEdges(void) const
{
    return m_halfEdges.size();
}

int Mesh::numberOfFaces(void) const
{
    return m_faces.size();
}

int Mesh::numberOfTriangles(void) const
{
    int count = 0;

    for(int i = 0; i < m_faces.size(); ++i) {
        if(m_faces[i]->edge()->next()->next()->next() == m_faces[i]->edge()) {
            ++count;
        }
    }

    return count;
}

int Mesh::numberOfQuads(void) const
{
    int count = 0;

    for(int i = 0; i < m_faces.size(); ++i) {
        if(m_faces[i]->edge()->next()->next()->next()->next() == m_faces[i]->edge()) {
            ++count;
        }
    }

    return count;
}

void Mesh::setHalfEdgeStatus(HalfEdge::Status status, bool overwrite)
{
    for(int i = 0; i < m_halfEdges.size(); ++i) {
        m_halfEdges[i]->setStatus(status, overwrite);
    }
}

Vector3D Mesh::centroid(void) const
{
    return m_vertexTotal / (double)m_vertices.size();
}

QPair<Vector3D, Vector3D> Mesh::boundingBox(void) const
{
    return m_boundingBox;
}

QList<Vertex *>& Mesh::vertices(void)
{
    return m_vertices;
}

QList<HalfEdge *>& Mesh::halfEdges(void)
{
    return m_halfEdges;
}

QList<Face *>& Mesh::faces(void)
{
    return m_faces;
}

void Mesh::update(void)
{
    updateNormals();
    updateHalfEdgeStatus();
    updateStatistics();
}

void Mesh::clear(void)
{
    // delete vertices
    while(!m_vertices.isEmpty()) {
        delete m_vertices.takeFirst();
    }

    // delete halfedges
    while(!m_halfEdges.isEmpty()) {
        delete m_halfEdges.takeFirst();
    }

    // delete faces
    while(!m_faces.isEmpty()) {
        delete m_faces.takeFirst();
    }

    m_boundingBox.first = Vector3D(-1, -1, -1);
    m_boundingBox.second = Vector3D(1, 1, 1);
    m_vertexTotal = Vector3D();
}

bool Mesh::isEmpty(void) const
{
    return m_vertices.isEmpty() && m_halfEdges.isEmpty() && m_faces.isEmpty();
}

bool Mesh::isConsistent(void) const
{
    Q_FOREACH(const Vertex *v, m_vertices) {
        if(!v->isConsistent()) {
            return false;
        }
    }

    Q_FOREACH(const HalfEdge *he, m_halfEdges) {
        if(!he->isConsistent()) {
            return false;
        }
    }

    Q_FOREACH(const Face *f, m_faces) {
        if(!f->isConsistent()) {
            return false;
        }
    }

    return true;
}

Vertex* Mesh::vertex(uint index)
{
    return m_vertices[index];
}

HalfEdge* Mesh::halfEdge(uint index)
{
    return m_halfEdges[index];
}

Face* Mesh::face(uint index)
{
    return m_faces[index];
}

void Mesh::insert(Vertex *v)
{
    if(v && !m_vertices.contains(v)) {
        m_vertices.append(v);
    }
}

void Mesh::insert(HalfEdge *he)
{
    if(he && !m_halfEdges.contains(he)) {
        m_halfEdges.append(he);
    }
}

void Mesh::insert(Face *f)
{
    if(f && !m_faces.contains(f)) {
        m_faces.append(f);
    }
}

void Mesh::remove(Vertex *v)
{
    if(v) {
        m_vertices.removeOne(v);
    }
}

void Mesh::remove(HalfEdge *he)
{
    if(he) {
        m_halfEdges.removeOne(he);
    }
}

void Mesh::remove(Face *f)
{
    if(f) {
        m_faces.removeOne(f);
    }
}

void Mesh::updateNormals(void)
{
    for(int i = 0; i < m_vertices.size(); ++i) {
        m_vertices[i]->setNormal(Vector3D());
    }

    for(int i = 0; i < m_faces.size(); ++i) {
        m_faces[i]->update();
    }
}

void Mesh::updateHalfEdgeStatus(void)
{
    for(int i = 0; i < m_halfEdges.size(); ++i) {
        if(m_halfEdges[i]->face() == Mesh::InfiniteFace ||
           m_halfEdges[i]->twin()->face() == Mesh::InfiniteFace) {
            m_halfEdges[i]->setStatus(HalfEdge::Boundary, true);
        } else {
            m_halfEdges[i]->setStatus(HalfEdge::Boundary, false);
        }
    }
}

void Mesh::updateStatistics(void)
{
    m_boundingBox.first = m_vertices[0]->coords();
    m_boundingBox.second = m_vertices[0]->coords();
    m_vertexTotal = m_vertices[0]->coords();

    for(int i = 1; i < m_vertices.size(); ++i) {
        m_vertexTotal += m_vertices[i]->coords();

        // get x min and max
        m_boundingBox.first.setX(qMin(m_vertices[i]->coords().x(),
                                      m_boundingBox.first.x()));
        m_boundingBox.second.setX(qMax(m_vertices[i]->coords().x(),
                                       m_boundingBox.second.x()));

        // get y min and max
        m_boundingBox.first.setY(qMin(m_vertices[i]->coords().y(),
                                      m_boundingBox.first.y()));
        m_boundingBox.second.setY(qMax(m_vertices[i]->coords().y(),
                                       m_boundingBox.second.y()));

        // get z min and max
        m_boundingBox.first.setZ(qMin(m_vertices[i]->coords().z(),
                                      m_boundingBox.first.z()));
        m_boundingBox.second.setZ(qMax(m_vertices[i]->coords().z(),
                                       m_boundingBox.second.z()));

    }
}
