#include <iostream>

#include <QString>

#include "formats/vtk.h"
#include "mesh.h"

using namespace std;

int main(void)
{
    QString filename = "../tests/example.vtk";

    Mesh *mesh = new Mesh();
    VTKReader reader(mesh, filename);
    reader.readData();

    if(!mesh->isConsistent()) {
        cout << "inconsistent mesh!" << endl;
    } else {
        cout << "at least this part works..." << endl;
    }

    return 0;
}
