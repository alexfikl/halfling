/*
 * meshioplugininterface.cpp
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#include <QFileInfo>

#include "meshioplugininterface.h"

bool MeshIOPluginInterface::isSupported(const QString &filename)
{
    QStringList supported = supportedMimetypes();
    QFileInfo finfo(filename);

    return supported.contains(finfo.suffix(), Qt::CaseInsensitive);
}

QString MeshIOPluginInterface::readLine(QTextStream &in,
                                        const QString commentMark)
{
    QString line;

    do {
        line = in.readLine();
        line = line.trimmed();
    } while(!in.atEnd() && (line.isEmpty() || line.startsWith(commentMark)));

    return line;
}

QList<int> MeshIOPluginInterface::toIntList(QString line)
{
    bool ok;
    QStringList words = line.split(" ");
    QList<int> ints;

    for(int i = 0; i < words.size(); ++i) {
        ints.append(words[i].toInt(&ok));
    }

    return ints;
}

QList<qreal> MeshIOPluginInterface::toDoubleList(QString line)
{
    bool ok;
    QStringList words = line.split(" ");
    QList<qreal> doubles;

    for(int i = 0; i < words.size(); ++i) {
        doubles.append(words[i].toDouble(&ok));
    }

    return doubles;
}
