/*
 * halfedge.cpp
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#include <QDebug>

#include "vertex.h"
#include "halfedge.h"

/*******************************************************
 *                      Constructors
 *******************************************************/

HalfEdge::HalfEdge():
    m_status(HalfEdge::NoStatus),
    m_origin(0), m_twin(0), m_next(0), m_face(0)
{
}

HalfEdge::HalfEdge(Vertex *origin,
                   HalfEdge *twin, HalfEdge *next,
                   Face *face):
    m_status(HalfEdge::NoStatus),
    m_origin(origin), m_twin(twin), m_next(next), m_face(face)
{
}

HalfEdge::~HalfEdge()
{
}

/*******************************************************
 *                      Operators
 *******************************************************/

HalfEdge& HalfEdge::operator=(HalfEdge &e)
{
    m_id = e.id();
    m_status = e.status();
    m_origin = e.origin();
    m_twin = e.twin();
    m_next = e.next();
    m_face = e.face();

    return (*this);
}

bool HalfEdge::operator==(HalfEdge &e)
{
    if(m_origin == e.origin() && m_twin == e.twin() &&
       m_next == e.next() && m_face == e.face()) {
           return true;
    }

    return false;
}

/*******************************************************
 *                      Setters
 *******************************************************/

void HalfEdge::setId(uint id)
{
    m_id = id;
}

void HalfEdge::setStatus(Status status, bool overwrite)
{
    if(overwrite == true) {
        m_status = (Status)(m_status | status);
    } else {
        m_status = (Status)(m_status & ~status);
    }
}

void HalfEdge::setOrigin(Vertex *origin)
{
    m_origin = origin;
}

void HalfEdge::setTwin(HalfEdge *twin)
{
    m_twin = twin;
}

void HalfEdge::setNext(HalfEdge *next)
{
    m_next = next;
}

void HalfEdge::setFace(Face *face)
{
    m_face = face;
}

/*******************************************************
 *                      Getters
 *******************************************************/
uint HalfEdge::id(void) const
{
    return m_id;
}

HalfEdge::Status HalfEdge::status(void) const
{
    return m_status;
}

bool HalfEdge::isStatusSet(Status status) const
{
    return (m_status & status) == status;
}

bool HalfEdge::isConsistent(void) const
{
    if(m_twin == NULL || m_origin == NULL || m_face == NULL || m_next == NULL)
        return false;

    if(m_twin->twin() == this || m_next->face() != m_face)
        return false;

    return true;
}

Vertex* HalfEdge::origin(void)
{
    return m_origin;
}

Vector3D HalfEdge::originCoords(void) const
{
    return m_origin->coords();
}

HalfEdge* HalfEdge::twin(void)
{
    return m_twin;
}

HalfEdge* HalfEdge::next(void)
{
    return m_next;
}

Face* HalfEdge::face(void)
{
    return m_face;
}
