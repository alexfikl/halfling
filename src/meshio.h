/*
 * meshio.h
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#ifndef __MESHIO_H__
#define __MESHIO_H__

#include <QString>
#include <QList>
#include <QFile>

#include "mesh.h"

class MeshIO
{
public:
    // constructors
    MeshIO();
    MeshIO(Mesh *mesh, const QString &filename);
    ~MeshIO();

    // some setters and getters
    void setFilename(const QString &filename);
    QString filename(void) const;

    void setMesh(Mesh *mesh);
    Mesh* mesh(void);

    // the important part
    QFile::FileError readData(void);
    QFile::FileError writeData(void);

private:
    void loadPlugins(void);

    // members
    Mesh *m_mesh;
    QString m_filename;
    QList<MeshIOPluginInterface *> m_plugins;
    MeshIOPluginInterface *m_io;
};

#endif // __MESHIO_H__
