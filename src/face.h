/*
 * face.h
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#ifndef __FACE_H__
#define __FACE_H__

// Qt includes
#include <QtGlobal>
#include <Vector3D>

class HalfEdge;

class Face
{
public:
    // constructors
    Face();
    Face(HalfEdge *edge);
    ~Face();

    // operators
    Face& operator=(Face &face);

    // setters and getters
    void setId(uint id);
    uint id(void) const;
    void setEdge(HalfEdge *edge);
    HalfEdge* edge(void);
    Vector3D normal(void) const;

    // others
    int edgeCount(void) const;
    int update(void);
    bool isConsistent(void) const;

private:

    uint m_id;

    Vector3D m_normal;
    HalfEdge *m_edge;
};

#endif
