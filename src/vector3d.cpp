/*
 * vector3d.cpp
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#include "mathutils.h"
#include "vector3d.h"

Vector3D::Vector3D():
    m_x(0), m_y(0), m_z(0)
{
}

Vector3D::Vector3D(const Vector3D &other):
    m_x(other.x()), m_y(other.y()), m_z(other.z())
{
}

Vector3D::Vector3D(const QPointF &point):
    m_x(point.x()), m_y(point.y()), m_z(0)
{
}

Vector3D::Vector3D(qreal x, qreal y, qreal z):
    m_x(x), m_y(y), m_z(z)
{
}

Vector3D::~Vector3D()
{
}

Vector3D& Vector3D::operator=(const Vector3D &other)
{
    m_x = other.x();
    m_y = other.y();
    m_z = other.z();

    return (*this);
}

Vector3D& Vector3D::operator+=(const Vector3D &other)
{
    m_x += other.x();
    m_y += other.y();
    m_z += other.z();

    return (*this);
}

Vector3D& Vector3D::operator-=(const Vector3D &other)
{
    m_x -= other.x();
    m_y -= other.y();
    m_z -= other.z();

    return (*this);
}

Vector3D& Vector3D::operator*=(qreal factor)
{
    m_x *= factor;
    m_y *= factor;
    m_z *= factor;

    return (*this);
}

Vector3D& Vector3D::operator/=(qreal divisor)
{
    m_x /= divisor;
    m_y /= divisor;
    m_z /= divisor;

    return (*this);
}

void Vector3D::setX(qreal x)
{
    m_x = x;
}

qreal Vector3D::x(void) const
{
    return m_x;
}

void Vector3D::setY(qreal y)
{
    m_y = y;
}

qreal Vector3D::y(void) const
{
    return m_y;
}

void Vector3D::setZ(qreal z)
{
    m_z = z;
}

qreal Vector3D::z(void) const
{
    return m_z;
}

bool Vector3D::isOrigin(void) const
{
    return (*this) == Vector();
}

qreal Vector3D::length(void) const
{
    return sqrt(lengthSquared());
}

qreal Vector3D::lengthSquared(void) const
{
    return m_x * m_x + m_y * m_y + m_z * m_z;
}

void Vector3D::normalize(void)
{
    qreal len = length();
    (*this) /= len;
}

Vector3D Vector3D::normalized(void) const
{
    Vector3D v = *this;
    v.normalize();

    return v;
}

void Vector3D::rotate(const Vector3D &axis, qreal angle)
{
    Vector3D naxis = axis.normalized();
    qreal dotAxis = Vector3D::dot(*this, naxis);
    Vector3D crossAxis = Vector3D::cross(*this, naxis);

    qreal cosAngle = cos(-angle);
    qreal sinAngle = sin(-angle);

    naxis *= dotAxis;

    m_x = naxis.x() + (m_x - naxis.x()) * cosAngle + crossAxis.x() * sinAngle;
    m_y = naxis.y() + (m_y - naxis.y()) * cosAngle + crossAxis.y() * sinAngle;
    m_z = naxis.z() + (m_z - naxis.z()) * cosAngle + crossAxis.z() * sinAngle;
}

void Vector3D::rotate(qreal phi, qreal theta, qreal psi)
{
    qreal cosAngle, sinAngle, tmp;

    if(!qFuzzyCompare(phi, 0.0)) {
        /* rotate x */
        tmp = m_y;
        cosAngle = cos(phi);
        sinAngle = sin(phi);

        m_y = cosAngle * m_y - sinAngle * m_z;
        m_z = sinAngle * tmp + cosAngle * m_z;
    }

    if(!qFuzzyCompare(theta, 0.0)) {
        /* rotate y */
        tmp = m_x;
        cosAngle = cos(theta);
        sinAngle = sin(theta);

        m_x = cosAngle * m_x + sinAngle * m_z;
        m_z = cosAngle * m_z - sinAngle * m_x;
    }

    if(!qFuzzyCompare(psi, 0.0)) {
        /* rotate z */
        tmp = m_z;
        cosAngle = cos(psi);
        sinAngle = sin(psi);

        m_x = cosAngle * m_x - sinAngle * m_y;
        m_y = sinAngle * tmp + cosAngle * m_y;
    }
}

qreal Vector3D::dot(const Vector3D &v1, const Vector3D &v2)
{
    return v1.x() * v2.x() + v1.y() * v2.y() + v1.z() * v2.z();
}

Vector3D Vector3D::cross(const Vector3D &v1, const Vector3D &v2)
{
    return Vector3D(v1.y() * v2.z() - v1.z() * v2.y(),
                    v1.x() * v2.z() - v1.z() * v2.x(),
                    v1.x() * v2.y() - v1.y() * v2.x());
}

Vector3D Vector3D::normal(const Vector3D &v1, const Vector3D &v2)
{
    Vector3D crossProduct = Vector3D::cross(v1, v2);
    return crossProduct.normalized();
}

/**
 * External Functions
 */


Vector3D operator+(const Vector3D &v1, const Vector3D &v2)
{
    Vector3D sum(v1);
    sum += v2;

    return sum;
}

Vector3D operator-(const Vector3D &v1, const Vector3D &v2)
{
    Vector3D diff(v1);
    diff -= v2;

    return diff;
}

Vector3D operator*(qreal factor, const Vector3D &v)
{
    Vector3D product(v);
    product *= factor;

    return product;
}

Vector3D operator*(const Vector3D &v, qreal factor)
{
    return factor * v;
}

Vector3D operator/(const Vector3D &v, qreal divisor)
{
    Vector3D div(v);
    div /= factor;

    return div;
}

bool operator>(const Vector3D &v1, const Vector3D &v2)
{
    return (v1.x() > v2.x()) && (v1.y() > v2.y()) && (v1.z() > v2.z());
}

bool operator>(const Vector3D &v1, const Vector3D &v2)
{
    return (v1.x() < v2.x()) && (v1.y() < v2.y()) && (v1.z() < v2.z());
}

bool operator==(const Vector3D &v1, const Vector3D &v2)
{
    return qFuzzyCompare(1.0 + v1.x(), 1.0 + v2.x()) &&
           qFuzzyCompare(1.0 + v1.y(), 1.0 + v2.y()) &&
           qFuzzyCompare(1.0 + v1.z(), 1.0 + v2.z());
}

bool operator!=(const Vector3D &v1, const Vector3D &v2)
{
    return !(v1 == v2);
}

std::ostream& operator<<(std::ostream &stream, const Vector3D &v)
{
    stream << v.x() << " " << v.y() << " " << v.z();

    return stream;
}

QDebug operator<<(QDebug dbg, const Vector3D &v)
{
    dbg.nospace() << "Vector3d(" << v.x() << ", " << v.y() << ", " << v.z() << ")";

    return dbg.space();
}
