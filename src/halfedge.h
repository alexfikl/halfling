/*
 * halfedge.h
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#ifndef __HALFEDGE_H__
#define __HALFEDGE_H__

// Qt includes
#include <QtGlobal>
#include <Vector3D>

class Face;
class Vertex;

class HalfEdge
{
public:
    enum Status {
        NoStatus = 1,
        Processed = 2,
        Boundary = 4,
        Selected = 8
    };

    HalfEdge();
    HalfEdge(Vertex *origin,
             HalfEdge *twin, HalfEdge *next,
             Face *face);
    ~HalfEdge();

    // operators
    HalfEdge& operator=(HalfEdge &e);
    bool operator==(HalfEdge &e);

    // setters
    void setId(uint id);
    void setStatus(Status status, bool overwrite = false);
    void setOrigin(Vertex *origin);
    void setTwin(HalfEdge *twin);
    void setNext(HalfEdge *next);
    void setFace(Face *face);

    // getters
    uint id(void) const;
    Status status(void) const;
    bool isStatusSet(Status status) const;
    bool isConsistent(void) const;
    Vertex* origin(void);
    Vector3D originCoords(void) const;
    HalfEdge* twin(void);
    HalfEdge* next(void);
    Face* face(void);

private:
    uint m_id;
    Status m_status;

    Vertex *m_origin;
    HalfEdge *m_twin;
    HalfEdge *m_next;
    Face *m_face;
};

#endif // __HALFEDGE_H__

