/*
 * vertex.h
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#ifndef __VERTEX_H__
#define __VERTEX_H__

// Qt includes
#include <QtGlobal>
#include <Vector3D>

class HalfEdge;

class Vertex
{
public:
    Vertex();
    Vertex(const Vertex &other);
    Vertex(qreal x, qreal y, qreal z, HalfEdge *leaving = 0);
    ~Vertex();

    // operators
    Vertex& operator=(const Vertex &other);
    bool operator==(const Vertex &other) const;

    // setters
    void setId(uint id);
    void setX(qreal x);
    void setY(qreal y);
    void setZ(qreal z);
    void setCoords(const Vector3D &coords);
    void setNormal(const Vector3D &normal);
    void setLeavingEdge(HalfEdge *leaving);

    // getters
    uint id(void) const;
    qreal x(void) const;
    qreal y(void) const;
    qreal z(void) const;
    Vector3D coords(void) const;
    Vector3D normal(void) const;
    HalfEdge* leaving(void) const;

    // others
    void scale(qreal scale);
    bool isConsistent(void) const;

    // static functions
    static qreal distance(const Vertex &u, const Vertex &v);
    static Vector3D direction(const Vertex &u, const Vertex &v);

private:
    uint m_id;

    Vector3D m_coords;
    Vector3D m_normal;

    HalfEdge *m_leaving;
};

#endif // __VERTEX_H__
