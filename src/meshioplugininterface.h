/*
 * meshioplugininterface.h
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#ifndef __MESHIOPLUGININTERFACE_H__
#define __MESHIOPLUGININTERFACE_H__

#include <QString>
#include <QList>
#include <QFile>
#include <QTextStream>

#include "mesh.h"

class MeshIOPluginInterface
{
public:
    virtual ~MeshIOPluginInterface();

    virtual QString name(void) const = 0;
    virtual QStringList supportedMimetypes(void) const = 0;
    virtual bool isSupported(const QString &filename);
    virtual QFile::FileError readData(Mesh *m, const QString &filename) = 0;
    virtual QFile::FileError writeData(Mesh *m, const QString &filename) = 0;

protected:
    QString readLine(QTextStream &in, const QString commentMark = "#");
    QList<int> toIntList(const QString &line);
    QList<qreal> toDoubleList(const QString &line);
};

#define MeshIOPluginInterfaceID "com.halfling.MeshIOPluginInterface/0.1"

Q_DECLARE_INTERFACE(MeshIOPluginInterface, MeshIOPluginInterfaceID)

#endif // __MESHIOPLUGININTERFACE_H__
