/*
 * vector3d.h
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#ifndef __VECTOR3D_H__
#define __VECTOR3D_H__

#include <iostream>

#include <QtGlobal>
#include <QPointF>
#include <QDebug>

class Vector3D
{
public:
    Vector3D();
    Vector3D(const Vector3D &other);
    Vector3D(const QPointF &point);
    Vector3D(qreal x, qreal y, qreal z);
    ~Vector3D();

    // operators
    Vector3D& operator=(const Vector3D &other);
    Vector3D& operator+=(const Vector3D &other);
    Vector3D& operator-=(const Vector3D &other);
    Vector3D& operator*=(qreal factor);
    Vector3D& operator/=(qreal divisor);

    // setters and getters
    void setX(qreal x);
    qreal x(void) const;
    void setY(qreal y);
    qreal y(void) const;
    void setZ(qreal z);
    qreal z(void) const;

    // some useful functions
    bool isOrigin(void) const;
    qreal length(void) const;
    qreal lengthSquared(void) const;
    void normalize(void);
    Vector3D normalized(void) const;
    void rotate(const Vector &axis, qreal angle);
    void rotate(qreal phi, qreal theta, qreal psi);

    static qreal dot(const Vector3D &v1, const Vector3D &v2);
    static Vector3D cross(const Vector3D &v1, const Vector3D &v2);
    static Vector3D normal(const Vector3D &v1, const Vector3D &v2);

private:
    qreal m_x;
    qreal m_y;
    qreal m_z;
};

// general operators
Vector3D operator+(const Vector3D &v1, const Vector3D &v2);
Vector3D operator-(const Vector3D &v1, const Vector3D &v2);
Vector3D operator*(qreal factor, const Vector3D &v);
Vector3D operator*(const Vector3D &v, qreal factor);
Vector3D operator/(const Vector3D &v, qreal divisor);
bool operator>(const Vector3D &v1, const Vector3D &v2);
bool operator<(const Vector3D &v1, const Vector3D &v2);
bool operator==(const Vector3D &v1, const Vector3D &v2);
bool operator!=(const Vector3D &v1, const Vector3D &v2);

// io
std::ostream& operator<<(std::ostream &stream, const Vector3D &v);
QDebug operator<<(QDebug dbg, const Vector3D &v);

#endif // __VECTOR3D_H__
