/*
 * mesh.h
 *
 * Copyright (c) 2013, Alexandru Fikl.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#ifndef __MESH_H__
#define __MESH_H__

// Qt includes
#include <QtGlobal>
#include <Vector3D>
#include <QPair>
#include <QList>

// halfling includes
#include "vertex.h"
#include "halfedge.h"
#include "face.h"

class Mesh
{
public:
    Mesh();
    Mesh(Mesh &other);
    ~Mesh();

    Mesh& operator=(Mesh &other);

    int numberOfVertices(void) const;
    int numberOfHalfEdges(void) const;
    int numberOfFaces(void) const;

    int numberOfTriangles(void) const;
    int numberOfQuads(void) const;

    void setHalfEdgeStatus(HalfEdge::Status status, bool overwrite = false);

    Vector3D centroid(void) const;
    QPair<Vector3D, Vector3D> boundingBox(void) const;
    QList<Vertex *>& vertices(void);
    QList<HalfEdge *>& halfEdges(void);
    QList<Face *>& faces(void);

    void update(void);
    void clear(void);
    bool isEmpty(void) const;
    bool isConsistent(void) const;

    Vertex* vertex(uint index);
    HalfEdge* halfEdge(uint index);
    Face* face(uint index);

    void insert(Vertex *v);
    void insert(HalfEdge *he);
    void insert(Face *f);

    void remove(Vertex *v);
    void remove(HalfEdge *he);
    void remove(Face *f);

    static const Face* InfiniteFace;

private:
    void updateNormals(void);
    void updateHalfEdgeStatus(void);
    void updateStatistics(void);

    QList<Vertex *> m_vertices;
    QList<HalfEdge *> m_halfEdges;
    QList<Face *> m_faces;

    QPair<Vector3D, Vector3D> m_boundingBox;
    Vector3D m_vertexTotal;
};

#endif
