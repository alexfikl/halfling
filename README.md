# Halfling

## Introduction

Ok, so this is supposed to be a nice Qt implementation of a library for
representing 3D meshes. Right now I'm doing it with triangular meshes in mind
but there's nothing really stopping it for working with any sort of meshes.

It uses the HalfEdge (i.e. [DCEL](http://en.wikipedia.org/wiki/Doubly_connected_edge_list))
data structure which is really nice as well and usually used for any sort of
polygonal mesh.

It's still, well, I just started writing it and I'm not that good at
writing stuff, so let your imagination wander.

The main idea is to have something small and extensible that supports reading
and writing to and from as many formats as boredom will allow me to look into.

And then, when that works nicely (probably before too, who has the patience
for waiting that long), use it for a nice OpenGL-based viewer.

## Installation

Installation is pretty straightforward. It uses CMake so you can just go:
```bash
mkdir build && cd build
cmake ..
```
and you're done, compilation issues aside.

## Reading

Some nice information about the HalfEdge data structure from around the
interwebs:
* http://www.flipcode.com/archives/The_Half-Edge_Data_Structure.shtml
* http://www.holmes3d.net/graphics/dcel/
* http://leonardofischer.com/dcel-data-structure-c-plus-plus-implementation/
